# Inherit Carbon GSM telephony parts
$(call inherit-product, vendor/carbon/config/gsm.mk)

# Inherit Carbon product configuration
$(call inherit-product, vendor/carbon/config/common.mk)

$(call inherit-product, device/sony/z3tc/full_z3tc.mk)

PRODUCT_BUILD_PROP_OVERRIDES += PRODUCT_NAME=SGP621
PRODUCT_BUILD_PROP_OVERRIDES += PRIVATE_BUILD_DESC="SGP621-user 6.0.1 23.5.A.1.291 3706784398 release-keys"

BUILD_FINGERPRINT := Sony/SGP621/SGP621:6.0.1/23.5.A.1.291/3706784398:user/release-keys

PRODUCT_NAME := carbon_z3tc
PRODUCT_DEVICE := z3tc

PRODUCT_PROPERTY_OVERRIDES += \
    ro.carbon.maintainer="evil-god"

# force sw keys
PRODUCT_PROPERTY_OVERRIDES += \
    qemu.hw.mainkeys=0
